## Table of Contents

- [Setup](#setup)
- [Architecture and Project overview](#project-overview-and-architecture)
- [Acknowledgement of the project](#acknowledgement)
- [Run the project](#run-project)
- [Production mode](#production)
- [Testing](#testing)

## Setup

The requirements to setup the project are:

- Required Node version: **>=v12.13.0**
- Required Npm version: **>=6.12.0**

Installing dependencies:

```sh
npm install
```

## Project overview and Architecture

The technologies and architecture used in this project are

- Typescript, React and SASS: I decided to use these technologies as I have experience with them.
- ESLint, Prettier and editorconfig: The combination of these three results in a cross IDE, autoformatted code style guidelines which improve developer time and also code quality necessary for any type of project.
- React-use-app: This library abstracts all types of complex configuration (transpiling, linting, styling, and bundling) to start your project, since this is a basic project the library will be good enough to use.
- **React.useReducer + Context API:** I wanted to implement a one-way data biding flow and I've implemented a Redux-like architecture so for this I've used both APIs to manage the state of App.

### Structure

- **`SystemMessages`**: They are actions that are hooked into the application lifecycle and are responsible for cross-controllers communication
- **`Components`**: They are dummy components that only render data that is passed in
- **`Controllers`**: The connection between `Components` and `State`, controllers bind data from the state to our components and they can also dispatch system messages
- **`Reducer`**: They are responsible for updating the state of the application
- **`Actions`**: They notify changes for the applications
- **`SideEffects`**: They are responsible for listening to system events and network requests, in general, they handle actions that doesn't affect the application directly
- **`StateManager`**: The responsible to connect all controllers reducers, actions, and system messages
- **`Models`**: Representative models from our server-side entities

### Caveats

- The **WA** prefix in the **components** stand for “Weather App"

### Patterns, Conventions, and Principles

- Single Responsibility Principle, the whole application is split into different responsibilities e.g `Components`, `Controllers`, `reducer`, `actions`, `side effects`, `selectors`.
- For our components dependencies (Material UI) that we use repetitively in the app we apply the Dependency Inversion Principle, Our components `Container` and `Paper` abstract the low-level modules `Material UI Container` and `Material UI Paper` from our high-level modules `Controllers` and `Components`, Material UI is loosely coupled to the weather app since changing the library would only affect our component abstractions.
- Interface Segregation Principle
- Open Closed Principle is applied for weather icons, it might not seem the case as we need to **modify** the WeatherIcon but that change is for **extension** and not for **modification** since we need to instantiate a new weather icon component somewhere, notice that wherever the `WeatherIcon` is used it doesn't need to change if we won't add another weather icon type
- Redux architecture

## Acknowledgement

- I've been pushing to master branch directly for this project, it would have been better to work on feature branches and merge them to master once finished but for the time scope, I think it's fine.
- This project is like a monorepo but without an actual monorepo manager, so I'm using npm in each packages client and server and we have a main package.json in the root directory that helps to install dependencies and to start the project
- There is no client cached data, Whenever the search city selection changes we always make requests to the server and it would have been nice to avoid requests if I already had the data in the client and only fetch new data in an interval time
- There are material UI components used in the `Components` folder that are not abstracted e.g. `Autocomplete`, `Button`, but that is fine because they don't repeat again other than the component that is using it.

## Aside projects

Here is a list of other projects where I've implemented more testing, my own CSS, a large list of items displayed, and Vanilla JS implementation if you would like to have a look:
https://github.com/joseaplwork/music-player-react
https://github.com/joseaplwork/million-books-list
https://github.com/joseaplwork/music-album-typescript

## Run project

To start the project run:

```sh
npm start
```

## Production

Visit: https://weather-app-adidas.herokuapp.com/

## Testing

The intention is to show how I do unit tests and I will be testing one part of the code as other tests would be repetitive.
Run the unit tests with the following command:

```sh
npm test
```
