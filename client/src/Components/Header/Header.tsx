import React from 'react';

import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';

import './Header.scss';

export function Header() {
  return (
    <AppBar className="wa-header" position="relative">
      <Toolbar>
        <Typography variant="h6">Weather app</Typography>
      </Toolbar>
    </AppBar>
  );
}
