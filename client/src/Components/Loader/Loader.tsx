import React from 'react';

import './Loader.scss';

type Props = {
  className: string;
};

export function Loader(props: Props) {
  const { className } = props;

  return <div className={`wa-spinner ${className}`} />;
}
