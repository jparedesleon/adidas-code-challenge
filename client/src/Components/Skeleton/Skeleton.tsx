import React from 'react';

type Props = {
  width: number;
  height: number;
  id: string;
};

export function Skeleton(props: React.PropsWithChildren<Props>) {
  const { width = 100, height = 100, children, id } = props;
  const primaryColor = '#ddd';
  const secondaryColor = '#eee';
  const speed = 2;

  return (
    <svg viewBox={`0 0 ${width} ${height}`} width="100%">
      <defs>
        <clipPath id={`skeleton_${id}_clip`}>{children}</clipPath>
        <linearGradient id="skeleton_fill">
          <stop offset="0%" stopColor={`${primaryColor}`}>
            <animate
              attributeName="offset"
              values="-2; 1"
              dur={`${speed}s`}
              repeatCount="indefinite"
            />
          </stop>
          <stop offset="50%" stopColor={`${secondaryColor}`}>
            <animate
              attributeName="offset"
              values="-1.5; 1.5"
              dur={`${speed}s`}
              repeatCount="indefinite"
            />
          </stop>
          <stop offset="100%" stopColor={`${primaryColor}`}>
            <animate
              attributeName="offset"
              values="-1; 2"
              dur={`${speed}`}
              repeatCount="indefinite"
            />
          </stop>
        </linearGradient>
      </defs>
      <rect
        fill="url(#skeleton_fill)"
        clipPath={`url(#skeleton_${id}_clip)`}
        x="0"
        y="0"
        height={`${height}`}
        width={`${width}`}
      />
    </svg>
  );
}
