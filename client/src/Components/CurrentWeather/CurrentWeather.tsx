import React from 'react';

import { Weather } from 'Models/Weather';
import { Paper } from 'Components/Paper';
import { WeatherIcon } from 'Components/Icons';
import { Container } from 'Components/Container';
import { Skeleton } from 'Components/Skeleton';

import './CurrentWeather.scss';

type CurrentWeatherStructure = {
  city: string;
  forecast: string;
  icon: Weather['icon'];
  temperature: string;
  date: string;
};

type Props = {
  data: CurrentWeatherStructure | null;
  isLoading: boolean;
};

const renderLoadingTemplate = () => {
  return (
    <div className="wa-current-weather">
      <Skeleton width={488} height={195} id="current_weather">
        <rect x="1" width="232" height="38" rx="3" />
        <rect x="1" y="55" width="161" height="21" rx="3" />
        <path d="M96 148C96 173.405 74.5097 194 48 194C21.4903 194 0 173.405 0 148C0 122.595 21.4903 102 48 102C74.5097 102 96 122.595 96 148Z" />
        <path d="M386 105C386 103.343 387.343 102 389 102H484C485.657 102 487 103.343 487 105V190C487 191.657 485.657 193 484 193H389C387.343 193 386 191.657 386 190V105Z" />
      </Skeleton>
    </div>
  );
};

const renderEmptyTemplate = () => {
  return (
    <div className="wa-current-weather wa-current-weather--empty">Select a city</div>
  );
};

const renderWeatherTemplate = (weather: CurrentWeatherStructure) => {
  const { city, forecast, temperature, date } = weather || {};

  return (
    <div className="wa-current-weather">
      <div className="wa-current-weather__city-name">
        <strong>{city}</strong>
      </div>
      <p>
        {forecast} - {date}
      </p>
      <div className="wa-current-weather__info">
        <div>
          <WeatherIcon icon={weather.icon} />
        </div>
        <div className="wa-current-weather__temperature">{temperature}</div>
      </div>
    </div>
  );
};

export function CurrentWeather(props: Props) {
  const { data, isLoading } = props;

  return (
    <Container maxWidth="sm">
      <Paper elevation={2}>
        {data && !isLoading && renderWeatherTemplate(data)}
        {!data && !isLoading && renderEmptyTemplate()}
        {isLoading && renderLoadingTemplate()}
      </Paper>
    </Container>
  );
}
