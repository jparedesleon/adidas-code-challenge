/* eslint-disable react/jsx-props-no-spreading */
import React from 'react';

import Autocomplete from '@material-ui/lab/Autocomplete';

import { City } from 'Models/City';
import { Paper } from 'Components/Paper';
import { Container } from 'Components/Container';
import { Loader } from 'Components/Loader';

import './SearchCity.scss';

type Props = {
  cities: Array<City>;
  isLoading: boolean;
  onCityChange: (option: any) => void;
};

export function SearchCity(props: Props) {
  const { cities, onCityChange, isLoading } = props;
  const handleCityChange = (_: any, option: City | string) => {
    onCityChange(option as City);
  };

  return (
    <Container className="wa-search-city" maxWidth="sm">
      <Paper
        className={`wa-search-city__paper ${
          isLoading ? 'wa-search-city__paper--loading' : ''
        }`}
      >
        <Autocomplete
          freeSolo
          disableClearable
          disabled={isLoading}
          onChange={handleCityChange}
          className="wa-search-city__autocomplete"
          options={cities}
          getOptionLabel={(option) => option.city}
          renderInput={(params) => (
            <div ref={params.InputProps.ref}>
              <input
                {...params.inputProps}
                placeholder="Search for city"
                className="wa-search-city__input"
              />
            </div>
          )}
        />
        {isLoading && <Loader className="wa-search-city__loader" />}
      </Paper>
    </Container>
  );
}
