import React from 'react';

import PaperMUI from '@material-ui/core/Paper';

type Props = {
  elevation?: number;
  className?: string;
};

export function Paper(props: React.PropsWithChildren<Props>) {
  const { children, elevation, className } = props;
  const classes = `wa-container ${className}`;

  return (
    <PaperMUI className={classes} elevation={elevation}>
      {children}
    </PaperMUI>
  );
}
