import React from 'react';

import { Weather } from 'Models/Weather';
import { WeatherIcon } from 'Components/Icons';

import './HourlyWeather.scss';

type HourlyWeatherStructure = {
  id: string;
  time: string;
  forecast: string;
  icon: Weather['icon'];
  temperature: string;
};

type Props = {
  data: Array<HourlyWeatherStructure> | null;
  weekDay: string;
  active?: boolean;
};

export function HourlyWeather(props: Props) {
  const { data, active, weekDay } = props;

  return (
    <div className={`wa-hourly-weather ${active ? 'wa-hourly-weather--active' : ''}`}>
      <div className="wa-hourly-weather__container">
        <div className="wa-hourly-weather__header">
          <span>{weekDay}</span>
          <span className="wa-hourly-weather__forecast-text">Forecast</span>
          <span>Temp(°C)</span>
        </div>
        {data ? (
          data.map((hour) => (
            <div key={hour.id} className="wa-hourly-weather__hour">
              <span>{hour.time}</span>
              <WeatherIcon icon={hour.icon} />
              <span>{hour.forecast}</span>
              <span>{hour.temperature}</span>
            </div>
          ))
        ) : (
          <div>Select a day</div>
        )}
      </div>
    </div>
  );
}
