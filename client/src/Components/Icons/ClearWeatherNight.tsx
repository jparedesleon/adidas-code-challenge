import React from 'react';

type Props = {
  width?: number;
  height?: number;
};

export function ClearWeatherNight(props: Props) {
  const { width, height = 50 } = props;

  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      version="1.1"
      width={width}
      height={height}
      viewBox="0 0 46 63"
    >
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M0.387514 59.9643C11.0769 54.9128 18.4671 44.074 18.4671 31.5182C18.4671 18.9623 11.0769 8.12363 0.387512 3.07203C4.49726 1.12984 9.09467 0.0430908 13.9472 0.0430908C31.4212 0.0430908 45.5866 14.135 45.5866 31.5182C45.5866 48.9014 31.4212 62.9933 13.9472 62.9933C9.09467 62.9933 4.49726 61.9065 0.387514 59.9643Z"
        fill="white"
      />
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M0.387514 59.9643C11.0769 54.9128 18.4671 44.074 18.4671 31.5182C18.4671 18.9623 11.0769 8.12363 0.387512 3.07203C4.49726 1.12984 9.09467 0.0430908 13.9472 0.0430908C31.4212 0.0430908 45.5866 14.135 45.5866 31.5182C45.5866 48.9014 31.4212 62.9933 13.9472 62.9933C9.09467 62.9933 4.49726 61.9065 0.387514 59.9643Z"
        fill="url(#id_clear_night1)"
        fillOpacity="0.3"
        style={{ mixBlendMode: 'overlay' }}
      />
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M0.387514 59.9643C11.0769 54.9128 18.4671 44.074 18.4671 31.5182C18.4671 18.9623 11.0769 8.12363 0.387512 3.07203C4.49726 1.12984 9.09467 0.0430908 13.9472 0.0430908C31.4212 0.0430908 45.5866 14.135 45.5866 31.5182C45.5866 48.9014 31.4212 62.9933 13.9472 62.9933C9.09467 62.9933 4.49726 61.9065 0.387514 59.9643Z"
        fill="#3F8AE9"
        fillOpacity="0.5"
      />
      <defs>
        <linearGradient
          id="id_clear_night1"
          x1="0.387512"
          y1="0.0430908"
          x2="0.387512"
          y2="62.9933"
          gradientUnits="userSpaceOnUse"
        >
          <stop stopColor="white" stopOpacity="0.5" />
          <stop offset="1" stopOpacity="0.5" />
        </linearGradient>
      </defs>
    </svg>
  );
}
