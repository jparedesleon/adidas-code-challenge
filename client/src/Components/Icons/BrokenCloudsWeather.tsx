import React from 'react';

type Props = {
  width?: number;
  height?: number;
};

export function BrokenCloudsWeather(props: Props) {
  const { width, height = 50 } = props;

  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      version="1.1"
      width={width}
      height={height}
      viewBox="0 0 104 66"
    >
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M94.4003 36.9971C99.3656 36.4563 103.32 32.2508 103.32 27.0978C103.32 22.4305 100.078 18.3846 95.7769 17.5068C94.5992 7.78011 86.5153 0.177185 76.7103 0.177185C69.8147 0.177185 63.7703 3.86669 60.3789 9.46045C60.2951 9.45883 60.2118 9.43557 60.1274 9.43557C52.8562 9.43557 46.9622 15.7605 46.9622 23.2163C46.9622 30.4104 52.4501 36.4563 59.3673 36.9971H94.4003Z"
        fill="#B8C7D8"
      />
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M94.4003 36.9971C99.3656 36.4563 103.32 32.2508 103.32 27.0978C103.32 22.4305 100.078 18.3846 95.7769 17.5068C94.5992 7.78011 86.5153 0.177185 76.7103 0.177185C69.8147 0.177185 63.7703 3.86669 60.3789 9.46045C60.2951 9.45883 60.2118 9.43557 60.1274 9.43557C52.8562 9.43557 46.9622 15.7605 46.9622 23.2163C46.9622 30.4104 52.4501 36.4563 59.3673 36.9971H94.4003Z"
        fill="url(#id_broken_clouds1)"
        fillOpacity="0.2"
      />
      <g filter="url(#id_broken_clouds)">
        <path
          fillRule="evenodd"
          clipRule="evenodd"
          d="M26.7118 51.8435H66.8138C74.1814 51.8435 80.1827 45.6548 80.1827 38.3C80.1827 32.1113 76.1189 26.9393 70.4035 25.2539C70.2522 19.7387 65.685 15.3361 60.0599 15.3361C58.425 15.3361 56.8846 15.7328 55.5094 16.3987C53.0769 10.702 47.3671 6.70975 40.7104 6.70975C33.4518 6.70975 27.3213 11.4693 25.3365 17.9958C16.7569 19.0186 10.1814 26.1261 10.1814 34.8696C10.1814 44.0304 17.3074 51.8435 26.7118 51.8435Z"
          fill="white"
        />
      </g>
      <defs>
        <filter
          id="id_broken_clouds"
          x="0.181396"
          y="0.709747"
          width="90.0014"
          height="65.1338"
          filterUnits="userSpaceOnUse"
          colorInterpolationFilters="sRGB"
        >
          <feFlood floodOpacity="0" result="BackgroundImageFix" />
          <feColorMatrix
            in="SourceAlpha"
            type="matrix"
            values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"
          />
          <feOffset dy="4" />
          <feGaussianBlur stdDeviation="5" />
          <feColorMatrix
            type="matrix"
            values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.14 0"
          />
          <feBlend mode="normal" in2="BackgroundImageFix" result="effect1_dropShadow" />
          <feBlend
            mode="normal"
            in="SourceGraphic"
            in2="effect1_dropShadow"
            result="shape"
          />
        </filter>
        <linearGradient
          id="id_broken_clouds1"
          x1="46.9622"
          y1="0.177185"
          x2="46.9622"
          y2="36.9971"
          gradientUnits="userSpaceOnUse"
        >
          <stop stopOpacity="0.01" />
          <stop offset="1" stopOpacity="0.5" />
        </linearGradient>
      </defs>
    </svg>
  );
}
