import React from 'react';

type Props = {
  width?: number;
  height?: number;
};

export function ThunderstormWeather(props: Props) {
  const { width, height = 50 } = props;

  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      version="1.1"
      width={width}
      height={height}
      viewBox="0 0 104 67"
    >
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M59.3599 37.7502H94.3929C99.3581 37.2094 103.312 33.0039 103.312 27.851C103.312 23.1836 100.07 19.1377 95.7695 18.26C94.5917 8.53322 86.5078 0.930298 76.7029 0.930298C69.8072 0.930298 63.7629 4.61981 60.3715 10.2136C60.2876 10.2119 60.2043 10.1887 60.1199 10.1887C52.8488 10.1887 46.9547 16.5136 46.9547 23.9695C46.9547 31.1635 52.4426 37.2094 59.3599 37.7502Z"
        fill="#959595"
      />
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M59.3599 37.7502H94.3929C99.3581 37.2094 103.312 33.0039 103.312 27.851C103.312 23.1836 100.07 19.1377 95.7695 18.26C94.5917 8.53322 86.5078 0.930298 76.7029 0.930298C69.8072 0.930298 63.7629 4.61981 60.3715 10.2136C60.2876 10.2119 60.2043 10.1887 60.1199 10.1887C52.8488 10.1887 46.9547 16.5136 46.9547 23.9695C46.9547 31.1635 52.4426 37.2094 59.3599 37.7502Z"
        fill="url(#id_thunderstorm1)"
        fillOpacity="0.2"
      />
      <g filter="url(#id_thunderstorm)">
        <path
          fillRule="evenodd"
          clipRule="evenodd"
          d="M26.7043 52.5967H66.8063C74.1739 52.5967 80.1753 46.4079 80.1753 39.0532C80.1753 32.8645 76.1114 27.6925 70.3961 26.0071C70.2447 20.4918 65.6776 16.0893 60.0524 16.0893C58.4176 16.0893 56.8772 16.4859 55.5019 17.1519C53.0694 11.4551 47.3596 7.46289 40.7029 7.46289C33.4443 7.46289 27.3138 12.2224 25.3291 18.7489C16.7494 19.7718 10.174 26.8792 10.174 35.6228C10.174 44.7835 17.2999 52.5967 26.7043 52.5967Z"
          fill="white"
        />
      </g>
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M41.7629 35.3748L36.8696 49.2469H44.5142L39.7286 62.6928L54.6668 43.5755L46.4343 43.3925L49.3008 35.559L41.7629 35.3748Z"
        fill="#FCC02E"
      />
      <defs>
        <filter
          id="id_thunderstorm"
          x="0.17395"
          y="1.46289"
          width="90.0014"
          height="65.1338"
          filterUnits="userSpaceOnUse"
          colorInterpolationFilters="sRGB"
        >
          <feFlood floodOpacity="0" result="BackgroundImageFix" />
          <feColorMatrix
            in="SourceAlpha"
            type="matrix"
            values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"
          />
          <feOffset dy="4" />
          <feGaussianBlur stdDeviation="5" />
          <feColorMatrix
            type="matrix"
            values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.14 0"
          />
          <feBlend mode="normal" in2="BackgroundImageFix" result="effect1_dropShadow" />
          <feBlend
            mode="normal"
            in="SourceGraphic"
            in2="effect1_dropShadow"
            result="shape"
          />
        </filter>
        <linearGradient
          id="id_thunderstorm1"
          x1="46.9547"
          y1="0.930298"
          x2="46.9547"
          y2="37.7502"
          gradientUnits="userSpaceOnUse"
        >
          <stop stopOpacity="0.01" />
          <stop offset="1" stopOpacity="0.5" />
        </linearGradient>
      </defs>
    </svg>
  );
}
