import React from 'react';

type Props = {
  width?: number;
  height?: number;
};

export function ScatteredCloudsWeather(props: Props) {
  const { width, height = 50 } = props;

  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      version="1.1"
      width={width}
      height={height}
      viewBox="0 0 87 62"
    >
      <g filter="url(#component_scattered_clouds)">
        <path
          fillRule="evenodd"
          clipRule="evenodd"
          d="M24.6671 49.133H64.7691C72.1367 49.133 78.1381 42.9443 78.1381 35.5895C78.1381 29.4008 74.0742 24.2288 68.3588 22.5434C68.2075 17.0282 63.6403 12.6256 58.0152 12.6256C56.3804 12.6256 54.8399 13.0223 53.4647 13.6882C51.0322 7.99141 45.3224 3.99921 38.6657 3.99921C31.4071 3.99921 25.2766 8.75875 23.2919 15.2852C14.7122 16.3081 8.13672 23.4156 8.13672 32.1591C8.13672 41.3198 15.2627 49.133 24.6671 49.133Z"
          fill="white"
        />
      </g>
      <defs>
        <filter
          id="component_scattered_clouds"
          x="0.136719"
          y="-0.000793457"
          width="86.0014"
          height="61.1338"
          filterUnits="userSpaceOnUse"
          colorInterpolationFilters="sRGB"
        >
          <feFlood floodOpacity="0" result="BackgroundImageFix" />
          <feColorMatrix
            in="SourceAlpha"
            type="matrix"
            values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"
          />
          <feOffset dy="4" />
          <feGaussianBlur stdDeviation="4" />
          <feColorMatrix
            type="matrix"
            values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.2 0"
          />
          <feBlend mode="normal" in2="BackgroundImageFix" result="effect1_dropShadow" />
          <feBlend
            mode="normal"
            in="SourceGraphic"
            in2="effect1_dropShadow"
            result="shape"
          />
        </filter>
      </defs>
    </svg>
  );
}
