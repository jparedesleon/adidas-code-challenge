import React from 'react';

type Props = {
  width?: number;
  height?: number;
};

export function SnowWeather(props: Props) {
  const { width, height = 50 } = props;

  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      version="1.1"
      width={width}
      height={height}
      viewBox="0 0 104 91"
    >
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M59.5048 37.7411H94.5378C99.503 37.2003 103.457 32.9948 103.457 27.8419C103.457 23.1745 100.215 19.1286 95.9144 18.2509C94.7366 8.52413 86.6527 0.921204 76.8478 0.921204C69.9521 0.921204 63.9078 4.61071 60.5164 10.2045C60.4325 10.2028 60.3492 10.1796 60.2648 10.1796C52.9936 10.1796 47.0996 16.5045 47.0996 23.9604C47.0996 31.1544 52.5875 37.2003 59.5048 37.7411Z"
        fill="#B6D6FA"
      />
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M59.5048 37.7411H94.5378C99.503 37.2003 103.457 32.9948 103.457 27.8419C103.457 23.1745 100.215 19.1286 95.9144 18.2509C94.7366 8.52413 86.6527 0.921204 76.8478 0.921204C69.9521 0.921204 63.9078 4.61071 60.5164 10.2045C60.4325 10.2028 60.3492 10.1796 60.2648 10.1796C52.9936 10.1796 47.0996 16.5045 47.0996 23.9604C47.0996 31.1544 52.5875 37.2003 59.5048 37.7411Z"
        fill="url(#id_snow1)"
        fillOpacity="0.1"
      />
      <g filter="url(#id_snow)">
        <path
          fillRule="evenodd"
          clipRule="evenodd"
          d="M26.8492 52.5876H66.9512C74.3188 52.5876 80.3202 46.3989 80.3202 39.0441C80.3202 32.8554 76.2563 27.6834 70.541 25.998C70.3896 20.4828 65.8225 16.0802 60.1973 16.0802C58.5625 16.0802 57.022 16.4768 55.6468 17.1428C53.2143 11.446 47.5045 7.4538 40.8478 7.4538C33.5892 7.4538 27.4587 12.2133 25.474 18.7398C16.8943 19.7627 10.3188 26.8701 10.3188 35.6137C10.3188 44.7744 17.4448 52.5876 26.8492 52.5876Z"
          fill="white"
        />
      </g>
      <circle cx="84.8188" cy="56.4212" r="4.5" fill="white" />
      <circle cx="23.8188" cy="77.4212" r="4.5" fill="white" />
      <circle cx="89.8188" cy="88.4212" r="2.5" fill="white" />
      <circle cx="48.8188" cy="61.4212" r="2.5" fill="white" />
      <defs>
        <filter
          id="id_snow"
          x="0.318848"
          y="1.4538"
          width="90.0014"
          height="65.1338"
          filterUnits="userSpaceOnUse"
          colorInterpolationFilters="sRGB"
        >
          <feFlood floodOpacity="0" result="BackgroundImageFix" />
          <feColorMatrix
            in="SourceAlpha"
            type="matrix"
            values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"
          />
          <feOffset dy="4" />
          <feGaussianBlur stdDeviation="5" />
          <feColorMatrix
            type="matrix"
            values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.14 0"
          />
          <feBlend mode="normal" in2="BackgroundImageFix" result="effect1_dropShadow" />
          <feBlend
            mode="normal"
            in="SourceGraphic"
            in2="effect1_dropShadow"
            result="shape"
          />
        </filter>
        <linearGradient
          id="id_snow1"
          x1="47.0996"
          y1="0.921204"
          x2="47.0996"
          y2="37.7411"
          gradientUnits="userSpaceOnUse"
        >
          <stop stopOpacity="0.01" />
          <stop offset="1" stopOpacity="0.5" />
        </linearGradient>
      </defs>
    </svg>
  );
}
