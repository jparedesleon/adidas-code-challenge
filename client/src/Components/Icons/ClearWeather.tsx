import React from 'react';

type Props = {
  width?: number;
  height?: number;
};

export function ClearWeather(props: Props) {
  const { width, height = 50 } = props;

  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      version="1.1"
      width={width}
      height={height}
      viewBox="0 0 70 67"
    >
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M34.8571 58.5136L24.2245 66.4722L20.2955 53.7772L7.02058 53.9595L11.296 41.3772L0.44928 33.7136L11.296 26.0499L7.02058 13.4676L20.2955 13.6499L24.2245 0.954956L34.8571 8.91357L45.4897 0.954956L49.4186 13.6499L62.6935 13.4676L58.4182 26.0499L69.2648 33.7136L58.4182 41.3772L62.6935 53.9595L49.4186 53.7772L45.4897 66.4722L34.8571 58.5136Z"
        fill="#FCC02E"
      />
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M34.8571 58.5136L24.2245 66.4722L20.2955 53.7772L7.02058 53.9595L11.296 41.3772L0.44928 33.7136L11.296 26.0499L7.02058 13.4676L20.2955 13.6499L24.2245 0.954956L34.8571 8.91357L45.4897 0.954956L49.4186 13.6499L62.6935 13.4676L58.4182 26.0499L69.2648 33.7136L58.4182 41.3772L62.6935 53.9595L49.4186 53.7772L45.4897 66.4722L34.8571 58.5136Z"
        fill="url(#id_clear1)"
        fillOpacity="0.3"
        style={{ mixBlendMode: 'overlay' }}
      />
      <g filter="url(#id_clear)">
        <ellipse cx="34.8571" cy="33.7136" rx="18.9836" ry="19.0038" fill="#FEE6AC" />
      </g>
      <defs>
        <filter
          id="id_clear"
          x="7.87347"
          y="10.7097"
          width="53.9672"
          height="54.0077"
          filterUnits="userSpaceOnUse"
          colorInterpolationFilters="sRGB"
        >
          <feFlood floodOpacity="0" result="BackgroundImageFix" />
          <feColorMatrix
            in="SourceAlpha"
            type="matrix"
            values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"
          />
          <feOffset dy="4" />
          <feGaussianBlur stdDeviation="4" />
          <feColorMatrix
            type="matrix"
            values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.1 0"
          />
          <feBlend mode="normal" in2="BackgroundImageFix" result="effect1_dropShadow" />
          <feBlend
            mode="normal"
            in="SourceGraphic"
            in2="effect1_dropShadow"
            result="shape"
          />
        </filter>
        <linearGradient
          id="id_clear1"
          x1="0.44928"
          y1="0.954956"
          x2="0.44928"
          y2="66.4722"
          gradientUnits="userSpaceOnUse"
        >
          <stop stopColor="white" stopOpacity="0.5" />
          <stop offset="1" stopOpacity="0.5" />
        </linearGradient>
      </defs>
    </svg>
  );
}
