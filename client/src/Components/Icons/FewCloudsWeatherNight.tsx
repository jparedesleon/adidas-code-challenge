import React from 'react';

type Props = {
  width?: number;
  height?: number;
};

export function FewCloudsWeatherNight(props: Props) {
  const { width, height = 50 } = props;

  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      version="1.1"
      width={width}
      height={height}
      viewBox="0 0 77 57"
    >
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M39.7508 49.538C48.4693 45.3918 54.497 36.4959 54.497 26.1906C54.497 15.8854 48.4693 6.98944 39.7508 2.84332C43.1028 1.24925 46.8526 0.3573 50.8104 0.3573C65.0626 0.3573 76.6163 11.9233 76.6163 26.1906C76.6163 40.458 65.0626 52.024 50.8104 52.024C46.8526 52.024 43.1028 51.132 39.7508 49.538Z"
        fill="white"
      />
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M39.7508 49.538C48.4693 45.3918 54.497 36.4959 54.497 26.1906C54.497 15.8854 48.4693 6.98944 39.7508 2.84332C43.1028 1.24925 46.8526 0.3573 50.8104 0.3573C65.0626 0.3573 76.6163 11.9233 76.6163 26.1906C76.6163 40.458 65.0626 52.024 50.8104 52.024C46.8526 52.024 43.1028 51.132 39.7508 49.538Z"
        fill="url(#id_few_clouds_night1)"
        fillOpacity="0.3"
        style={{ mixBlendMode: 'overlay' }}
      />
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M39.7508 49.538C48.4693 45.3918 54.497 36.4959 54.497 26.1906C54.497 15.8854 48.4693 6.98944 39.7508 2.84332C43.1028 1.24925 46.8526 0.3573 50.8104 0.3573C65.0626 0.3573 76.6163 11.9233 76.6163 26.1906C76.6163 40.458 65.0626 52.024 50.8104 52.024C46.8526 52.024 43.1028 51.132 39.7508 49.538Z"
        fill="#3F8AE9"
        fillOpacity="0.5"
      />
      <g filter="url(#id_few_clouds_night)">
        <path
          fillRule="evenodd"
          clipRule="evenodd"
          d="M20.6062 44.8973H49.1534C54.3982 44.8973 58.6703 40.5 58.6703 35.2743C58.6703 30.877 55.7774 27.2022 51.7088 26.0047C51.6011 22.086 48.3499 18.9578 44.3456 18.9578C43.1818 18.9578 42.0852 19.2397 41.1062 19.7128C39.3746 15.6651 35.31 12.8286 30.5714 12.8286C25.4042 12.8286 21.0401 16.2103 19.6273 20.8476C13.5197 21.5743 8.83887 26.6244 8.83887 32.8369C8.83887 39.3458 13.9116 44.8973 20.6062 44.8973Z"
          fill="white"
        />
      </g>
      <defs>
        <filter
          id="id_few_clouds_night"
          x="0.838867"
          y="8.82855"
          width="65.8315"
          height="48.0687"
          filterUnits="userSpaceOnUse"
          colorInterpolationFilters="sRGB"
        >
          <feFlood floodOpacity="0" result="BackgroundImageFix" />
          <feColorMatrix
            in="SourceAlpha"
            type="matrix"
            values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"
          />
          <feOffset dy="4" />
          <feGaussianBlur stdDeviation="4" />
          <feColorMatrix
            type="matrix"
            values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.2 0"
          />
          <feBlend mode="normal" in2="BackgroundImageFix" result="effect1_dropShadow" />
          <feBlend
            mode="normal"
            in="SourceGraphic"
            in2="effect1_dropShadow"
            result="shape"
          />
        </filter>
        <linearGradient
          id="id_few_clouds_night1"
          x1="39.7508"
          y1="0.3573"
          x2="39.7508"
          y2="52.024"
          gradientUnits="userSpaceOnUse"
        >
          <stop stopColor="white" stopOpacity="0.5" />
          <stop offset="1" stopOpacity="0.5" />
        </linearGradient>
      </defs>
    </svg>
  );
}
