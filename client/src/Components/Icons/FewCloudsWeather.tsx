import React from 'react';

type Props = {
  width?: number;
  height?: number;
};

export function FewCloudsWeather(props: Props) {
  const { width, height = 50 } = props;

  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      version="1.1"
      width={width}
      height={height}
      viewBox="0 0 84 68"
    >
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M55.1114 48.299L46.312 54.8855L43.0604 44.3793L32.0743 44.5301L35.6126 34.1172L26.636 27.7749L35.6126 21.4326L32.0743 11.0196L43.0604 11.1705L46.312 0.664305L55.1114 7.25074L63.9108 0.664305L67.1623 11.1705L78.1485 11.0196L74.6102 21.4326L83.5868 27.7749L74.6102 34.1172L78.1485 44.5301L67.1623 44.3793L63.9108 54.8855L55.1114 48.299Z"
        fill="#FCC02E"
      />
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M55.1114 48.299L46.312 54.8855L43.0604 44.3793L32.0743 44.5301L35.6126 34.1172L26.636 27.7749L35.6126 21.4326L32.0743 11.0196L43.0604 11.1705L46.312 0.664305L55.1114 7.25074L63.9108 0.664305L67.1623 11.1705L78.1485 11.0196L74.6102 21.4326L83.5868 27.7749L74.6102 34.1172L78.1485 44.5301L67.1623 44.3793L63.9108 54.8855L55.1114 48.299Z"
        fill="url(#paint0_linear)"
        fillOpacity="0.3"
        style={{ mixBlendMode: 'overlay' }}
      />
      <g filter="url(#id_few_clouds1)">
        <ellipse cx="55.1114" cy="27.7749" rx="15.4242" ry="15.4406" fill="#FEE6AC" />
      </g>
      <g filter="url(#id_few_clouds)">
        <path
          fillRule="evenodd"
          clipRule="evenodd"
          d="M49.1534 55.0927C54.3982 55.0927 58.6703 50.6954 58.6703 45.4696C58.6703 41.0724 55.7774 37.3976 51.7088 36.2001C51.6011 32.2813 48.3499 29.1532 44.3456 29.1532C43.1818 29.1532 42.0852 29.435 41.1062 29.9082C39.3746 25.8605 35.31 23.0239 30.5714 23.0239C25.4042 23.0239 21.0401 26.4057 19.6273 31.0429C13.5197 31.7697 8.83887 36.8198 8.83887 43.0323C8.83887 49.5412 13.9116 55.0927 20.6062 55.0927H49.1534Z"
          fill="white"
        />
      </g>
      <defs>
        <filter
          id="id_few_clouds1"
          x="31.6872"
          y="8.33426"
          width="46.8484"
          height="46.8812"
          filterUnits="userSpaceOnUse"
          colorInterpolationFilters="sRGB"
        >
          <feFlood floodOpacity="0" result="BackgroundImageFix" />
          <feColorMatrix
            in="SourceAlpha"
            type="matrix"
            values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"
          />
          <feOffset dy="4" />
          <feGaussianBlur stdDeviation="4" />
          <feColorMatrix
            type="matrix"
            values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.1 0"
          />
          <feBlend mode="normal" in2="BackgroundImageFix" result="effect1_dropShadow" />
          <feBlend
            mode="normal"
            in="SourceGraphic"
            in2="effect1_dropShadow"
            result="shape"
          />
        </filter>
        <filter
          id="id_few_clouds"
          x="0.838867"
          y="19.0239"
          width="65.8315"
          height="48.0687"
          filterUnits="userSpaceOnUse"
          colorInterpolationFilters="sRGB"
        >
          <feFlood floodOpacity="0" result="BackgroundImageFix" />
          <feColorMatrix
            in="SourceAlpha"
            type="matrix"
            values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"
          />
          <feOffset dy="4" />
          <feGaussianBlur stdDeviation="4" />
          <feColorMatrix
            type="matrix"
            values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.14 0"
          />
          <feBlend mode="normal" in2="BackgroundImageFix" result="effect1_dropShadow" />
          <feBlend
            mode="normal"
            in="SourceGraphic"
            in2="effect1_dropShadow"
            result="shape"
          />
        </filter>
        <linearGradient
          id="paint0_linear"
          x1="26.636"
          y1="-0.730865"
          x2="26.636"
          y2="53.4903"
          gradientUnits="userSpaceOnUse"
        >
          <stop stopColor="white" stopOpacity="0.5" />
          <stop offset="1" stopOpacity="0.5" />
        </linearGradient>
      </defs>
    </svg>
  );
}
