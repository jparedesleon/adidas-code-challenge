import React from 'react';

type Props = {
  width?: number;
  height?: number;
};

export function RainWeather(props: Props) {
  const { width, height = 50 } = props;

  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      version="1.1"
      width={width}
      height={height}
      viewBox="0 0 105 75"
    >
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M60.1715 37.8048H95.2045C100.17 37.264 104.124 33.0585 104.124 27.9055C104.124 23.2382 100.882 19.1923 96.5811 18.3145C95.4034 8.58778 87.3195 0.984863 77.5145 0.984863C70.6189 0.984863 64.5745 4.67437 61.1831 10.2681C61.0993 10.2665 61.0159 10.2433 60.9316 10.2433C53.6604 10.2433 47.7664 16.5682 47.7664 24.024C47.7664 31.2181 53.2543 37.264 60.1715 37.8048Z"
        fill="#B8C7D8"
      />
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M60.1715 37.8048H95.2045C100.17 37.264 104.124 33.0585 104.124 27.9055C104.124 23.2382 100.882 19.1923 96.5811 18.3145C95.4034 8.58778 87.3195 0.984863 77.5145 0.984863C70.6189 0.984863 64.5745 4.67437 61.1831 10.2681C61.0993 10.2665 61.0159 10.2433 60.9316 10.2433C53.6604 10.2433 47.7664 16.5682 47.7664 24.024C47.7664 31.2181 53.2543 37.264 60.1715 37.8048Z"
        fill="url(#id_rain1)"
        fillOpacity="0.2"
      />
      <g filter="url(#id_rain)">
        <path
          fillRule="evenodd"
          clipRule="evenodd"
          d="M27.516 52.6512H67.618C74.9856 52.6512 80.9869 46.4625 80.9869 39.1077C80.9869 32.919 76.9231 27.747 71.2077 26.0616C71.0564 20.5464 66.4892 16.1438 60.8641 16.1438C59.2292 16.1438 57.6888 16.5405 56.3136 17.2064C53.8811 11.5097 48.1713 7.51746 41.5146 7.51746C34.256 7.51746 28.1255 12.277 26.1407 18.8035C17.5611 19.8264 10.9856 26.9338 10.9856 35.6773C10.9856 44.8381 18.1116 52.6512 27.516 52.6512Z"
          fill="white"
        />
      </g>
      <ellipse cx="36.4948" cy="67.1228" rx="4.7459" ry="7.12644" fill="#F3F3F3" />
      <ellipse cx="53.6988" cy="61.7779" rx="2.37295" ry="3.56322" fill="#F3F3F3" />
      <defs>
        <filter
          id="id_rain"
          x="0.985596"
          y="1.51746"
          width="90.0014"
          height="65.1338"
          filterUnits="userSpaceOnUse"
          colorInterpolationFilters="sRGB"
        >
          <feFlood floodOpacity="0" result="BackgroundImageFix" />
          <feColorMatrix
            in="SourceAlpha"
            type="matrix"
            values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"
          />
          <feOffset dy="4" />
          <feGaussianBlur stdDeviation="5" />
          <feColorMatrix
            type="matrix"
            values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.14 0"
          />
          <feBlend mode="normal" in2="BackgroundImageFix" result="effect1_dropShadow" />
          <feBlend
            mode="normal"
            in="SourceGraphic"
            in2="effect1_dropShadow"
            result="shape"
          />
        </filter>
        <linearGradient
          id="id_rain1"
          x1="47.7664"
          y1="0.984863"
          x2="47.7664"
          y2="37.8048"
          gradientUnits="userSpaceOnUse"
        >
          <stop stopOpacity="0.01" />
          <stop offset="1" stopOpacity="0.5" />
        </linearGradient>
      </defs>
    </svg>
  );
}
