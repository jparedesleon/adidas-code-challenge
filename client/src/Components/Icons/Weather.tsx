import React from 'react';

import { Weather as WeatherType } from 'Models/Weather';

import { ClearWeather } from './ClearWeather';
import { ClearWeatherNight } from './ClearWeatherNight';
import { FewCloudsWeather } from './FewCloudsWeather';
import { FewCloudsWeatherNight } from './FewCloudsWeatherNight';
import { ScatteredCloudsWeather } from './ScatteredCloudsWeather';
import { MistWeather } from './MistWeather';
import { BrokenCloudsWeather } from './BrokenCloudsWeather';
import { ShowerRainWeather } from './ShowerRainWeather';
import { RainWeather } from './RainWeather';
import { ThunderstormWeather } from './ThunderstormWeather';
import { SnowWeather } from './SnowWeather';

type Props = {
  icon: WeatherType['icon'];
  width?: number;
  height?: number;
};
export function Weather(props: Props) {
  const { icon, width, height } = props;

  const icons = {
    '01d': <ClearWeather width={width} height={height} />,
    '01n': <ClearWeatherNight width={width} height={height} />,
    '02d': <FewCloudsWeather width={width} height={height} />,
    '02n': <FewCloudsWeatherNight width={width} height={height} />,
    '03d': <ScatteredCloudsWeather width={width} height={height} />,
    '03n': <ScatteredCloudsWeather width={width} height={height} />,
    '04d': <BrokenCloudsWeather width={width} height={height} />,
    '04n': <BrokenCloudsWeather width={width} height={height} />,
    '09d': <ShowerRainWeather width={width} height={height} />,
    '09n': <ShowerRainWeather width={width} height={height} />,
    '10d': <RainWeather width={width} height={height} />,
    '10n': <RainWeather width={width} height={height} />,
    '11d': <ThunderstormWeather width={width} height={height} />,
    '11n': <ThunderstormWeather width={width} height={height} />,
    '13d': <SnowWeather width={width} height={height} />,
    '13n': <SnowWeather width={width} height={height} />,
    '50d': <MistWeather width={width} height={height} />,
    '50n': <MistWeather width={width} height={height} />,
  };

  return icons[icon];
}
