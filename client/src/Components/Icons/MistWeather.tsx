import React from 'react';

type Props = {
  width?: number;
  height?: number;
};

export function MistWeather(props: Props) {
  const { width, height = 50 } = props;

  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      version="1.1"
      width={width}
      height={height}
      viewBox="0 0 67 52"
    >
      <path
        d="M2.04675 5.91756C9.49353 2.43933 18.0573 0.700213 26.2488 3.13497C31.0892 4.52627 35.1849 7.30885 40.0253 8.70015C48.2168 11.1349 56.7806 9.39579 64.2274 5.91756"
        stroke="#FBFBFB"
        strokeWidth="4"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        opacity="0.7"
        d="M2.04675 19.1217C9.49353 15.6435 18.0573 13.9044 26.2488 16.3391C31.0892 17.7304 35.1849 20.513 40.0253 21.9043C48.2168 24.3391 56.7806 22.6 64.2274 19.1217"
        stroke="#FBFBFB"
        strokeWidth="4"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        opacity="0.4"
        d="M2.04675 32.3259C9.49353 28.8477 18.0573 27.1085 26.2488 29.5433C31.0892 30.9346 35.1849 33.7172 40.0253 35.1085C48.2168 37.5432 56.7806 35.8041 64.2274 32.3259"
        stroke="#FBFBFB"
        strokeWidth="4"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        opacity="0.2"
        d="M2.04675 45.53C9.49353 42.0518 18.0573 40.3126 26.2488 42.7474C31.0892 44.1387 35.1849 46.9213 40.0253 48.3126C48.2168 50.7473 56.7806 49.0082 64.2274 45.53"
        stroke="#FBFBFB"
        strokeWidth="4"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </svg>
  );
}
