import React from 'react';

import ContainerMUI from '@material-ui/core/Container';

import './Container.scss';

type Props = {
  maxWidth?: 'lg' | 'md' | 'sm' | 'xl' | 'xs' | false;
  className?: string;
  children: React.ReactElement | Array<React.ReactElement>;
};

export function Container(props: Props) {
  const { children, maxWidth, className } = props;
  const classes = `wa-container ${className}`;

  return (
    <ContainerMUI className={classes} maxWidth={maxWidth}>
      {children}
    </ContainerMUI>
  );
}
