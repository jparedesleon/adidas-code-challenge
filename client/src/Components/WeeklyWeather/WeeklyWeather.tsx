import React from 'react';

import ButtonBase from '@material-ui/core/ButtonBase';

import { Weather } from 'Models/Weather';
import { Paper } from 'Components/Paper';
import { WeatherIcon } from 'Components/Icons';
import { Container } from 'Components/Container';
import { Skeleton } from 'Components/Skeleton';

import './WeeklyWeather.scss';

type WeeklyWeatherStructure = {
  id: number;
  day: string;
  weather: Weather;
  maxTemperature: string;
  minTemperature: string;
};
type OnWeatherClick = (id: number) => void;
type Props = {
  data: Array<WeeklyWeatherStructure>;
  isLoading: boolean;
  children: React.ReactElement;
  onWeatherClick: OnWeatherClick;
};

const renderEmptyTemplate = () => {
  return (
    <div className="wa-weekly-weather wa-weekly-weather--empty">
      <div>no data to show</div>
    </div>
  );
};

const renderLoadingTemplate = () => {
  return (
    <div className="wa-weekly-weather">
      <Skeleton width={520} height={95} id="weekly_weather">
        <rect width="53" height="11.2192" rx="3" />
        <rect y="79.7808" width="53" height="11.2192" rx="3" />
        <ellipse cx="26.5" cy="43.0069" rx="26.5" ry="23.0616" />
        <rect x="116" width="53" height="11.2192" rx="3" />
        <rect x="116" y="79.7808" width="53" height="11.2192" rx="3" />
        <ellipse cx="142.5" cy="43.0069" rx="26.5" ry="23.0616" />
        <rect x="232" width="53" height="11.2192" rx="3" />
        <rect x="232" y="79.7808" width="53" height="11.2192" rx="3" />
        <ellipse cx="258.5" cy="43.0069" rx="26.5" ry="23.0616" />
        <rect x="348" width="53" height="11.2192" rx="3" />
        <rect x="348" y="79.7808" width="53" height="11.2192" rx="3" />
        <ellipse cx="374.5" cy="43.0069" rx="26.5" ry="23.0616" />
        <rect x="464" width="53" height="11.2192" rx="3" />
        <rect x="464" y="79.7808" width="53" height="11.2192" rx="3" />
        <ellipse cx="490.5" cy="43.0069" rx="26.5" ry="23.0616" />
      </Skeleton>
    </div>
  );
};

const renderWeeklyWeather = (
  data: Array<WeeklyWeatherStructure>,
  onWeatherClick: OnWeatherClick,
) => {
  return (
    <div className="wa-weekly-weather">
      {data.map((week) => (
        <ButtonBase
          focusRipple
          key={week.id}
          onClick={() => {
            onWeatherClick(week.id);
          }}
        >
          <div className="wa-weekly-weather__day">
            <span>{week.day}</span>
            <WeatherIcon icon={week.weather.icon} />
            <span>{week.maxTemperature}</span>
            <span>{week.minTemperature}</span>
          </div>
        </ButtonBase>
      ))}
    </div>
  );
};

export function WeeklyWeather(props: Props) {
  const { data, children, isLoading, onWeatherClick } = props;
  const shouldRenderData = data.length > 0 && !isLoading;
  const shouldRenderEmpty = data.length === 0 && !isLoading;
  const shouldRenderLoading = isLoading;

  return (
    <Container maxWidth="sm">
      <Paper elevation={2}>
        {shouldRenderData && renderWeeklyWeather(data, onWeatherClick)}
        {shouldRenderEmpty && renderEmptyTemplate()}
        {shouldRenderLoading && renderLoadingTemplate()}
        {children}
      </Paper>
    </Container>
  );
}
