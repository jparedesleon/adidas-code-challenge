import { City } from './City';
import { HourWeather, HourWeatherJSON } from './HourWeather';

export interface DayWeather {
  id: number;
  date: Date;
  location: City;
  minTemperature: number;
  maxTemperature: number;
  hourlyTemperature: Array<HourWeather>;
}

export interface DayWeatherJSON {
  id: number;
  date: number;
  location: City;
  // eslint-disable-next-line camelcase
  min_temperature: number;
  // eslint-disable-next-line camelcase
  max_temperature: number;
  // eslint-disable-next-line camelcase
  hourly_temperature: Array<HourWeatherJSON>;
}
