import { DayWeather, DayWeatherJSON } from './DayWeather';
import { HourWeather, HourWeatherJSON } from './HourWeather';

export const normalizeHourWeatherJSON = (weather: HourWeatherJSON): HourWeather => {
  return {
    id: weather.id,
    hour: new Date(weather.hour * 1000),
    temperature: weather.temperature,
    weather: weather.weather,
  };
};

export const normalizeDayWeatherJSON = (weather: DayWeatherJSON): DayWeather => {
  return {
    id: weather.id,
    date: new Date(weather.date * 1000),
    location: weather.location,
    minTemperature: weather.min_temperature,
    maxTemperature: weather.max_temperature,
    hourlyTemperature: weather.hourly_temperature.map(normalizeHourWeatherJSON),
  };
};
