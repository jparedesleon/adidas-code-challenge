export interface City {
  id: number;
  city: string;
  country: string;
}

export type CityJSON = City;
