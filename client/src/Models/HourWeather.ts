import { Weather } from './Weather';

export interface HourWeather {
  id: string;
  hour: Date;
  temperature: number;
  weather: Weather;
}

export interface HourWeatherJSON {
  id: string;
  hour: number;
  temperature: number;
  weather: Weather;
}
