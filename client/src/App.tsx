import React from 'react';

import { StateManager } from 'StateManager';
import { Header } from 'Components/Header';
import { SearchCityController } from 'Controllers/SearchCityController';
import { CurrentWeatherController } from 'Controllers/CurrentWeatherController';
import { WeeklyWeatherController } from 'Controllers/WeeklyWeatherController';

function App() {
  return (
    <StateManager>
      <Header />
      <SearchCityController />
      <CurrentWeatherController />
      <WeeklyWeatherController />
    </StateManager>
  );
}

export default App;
