export const days = [
  'Sunday',
  'Monday',
  'Tuesday',
  'Wednesday',
  'Thursday',
  'Friday',
  'Saturday',
];

export function timeToHoursAndMinutes(time: Date) {
  let hours: number | string = time.getHours();
  let minutes: number | string = time.getMinutes();

  if (hours < 10) {
    hours = `0${hours}`;
  }

  if (minutes < 10) {
    minutes = `0${minutes}`;
  }

  return `${hours}:${minutes}`;
}

export function filterForFutureHours(time: Date) {
  const now = new Date();
  const nextDay = new Date(time);

  now.setHours(now.getHours(), 0, 0, 0);
  nextDay.setDate(nextDay.getDate() + 1);
  nextDay.setHours(0, 0, 0, 0);

  return now.getTime() <= time.getTime() && time.getTime() < nextDay.getTime();
}

export function isCurrentDate(date: Date) {
  const now = new Date();

  now.setHours(0, 0, 0, 0);

  return date.getTime() === now.getTime();
}

export function filterForNextFiveDays(time: Date) {
  const now = new Date();

  now.setHours(0, 0, 0, 0);

  const nextFiveDays = new Date(now.getTime() + 4 * 24 * 60 * 60 * 1000);

  nextFiveDays.setHours(0, 0, 0, 0);

  return time.getTime() >= now.getTime() && time.getTime() <= nextFiveDays.getTime();
}

export function getDayString(date: Date) {
  return days[date.getDay()];
}
