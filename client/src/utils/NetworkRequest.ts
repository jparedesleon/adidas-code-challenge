type RequestInfo = {
  method?: string;
  headers?: Record<string, string>;
  body?: Record<string, unknown>;
};

export class NetworkRequest {
  url: string;

  options?: RequestInfo;

  constructor(url: string, options?: RequestInfo) {
    this.url = url;
    this.options = options;
  }

  async sendRequest(): Promise<any> {
    const { url, options } = this;

    const response = await fetch(url, {
      method: 'GET',
      ...options,
      mode: 'cors',
      body: JSON.stringify(options?.body),
    });

    return response;
  }
}
