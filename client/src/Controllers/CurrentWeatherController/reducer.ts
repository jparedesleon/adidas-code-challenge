import { Actions } from 'StateManager';
import { DayWeather } from 'Models/DayWeather';
import { normalizeDayWeatherJSON } from 'Models/normalizeData';
import { isCurrentDate } from 'utils/time';

import type { Actions as LocalActions } from './actions';
import * as C from './constants';

export type CurrentWeatherState = {
  currentWeather: DayWeather | null;
  isLoading: boolean;
};

export type CurrentWeatherActions = LocalActions;
export const initialState: CurrentWeatherState = {
  currentWeather: null,
  isLoading: false,
};

export function reducer(state = initialState, action: Actions): CurrentWeatherState {
  switch (action.type) {
    case C.SET_CURRENT_WEATHER: {
      const weeklyWeather = action.payload!;

      const todaysWeahter = weeklyWeather.find((weather) => {
        const day = normalizeDayWeatherJSON(weather);

        return isCurrentDate(day.date);
      });

      return {
        ...state,
        isLoading: false,
        currentWeather: todaysWeahter ? normalizeDayWeatherJSON(todaysWeahter) : null,
      };
    }
    case C.SET_IS_LOADING: {
      return {
        ...state,
        isLoading: !!action.payload,
      };
    }
    default: {
      return state;
    }
  }
}
