import React, { useContext } from 'react';

import { StateContext } from 'StateManager';
import { CurrentWeather } from 'Components/CurrentWeather';

import { selectCurrentWeather, selectIsLoading } from './selectors';

export function CurrentWeatherController() {
  const { state } = useContext(StateContext);
  const currentWeather = selectCurrentWeather(state);
  const isLoading = selectIsLoading(state);

  return <CurrentWeather isLoading={isLoading} data={currentWeather} />;
}
