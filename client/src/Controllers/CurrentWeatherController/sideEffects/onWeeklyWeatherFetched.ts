import { Dispatcher, Actions, SYSTEM_MESSAGE } from 'StateManager';
import { DayWeatherJSON } from 'Models/DayWeather';

import { setCurrentWeather } from '../actions';

export async function onWeeklyWeatherFetched(action: Actions, dispatch: Dispatcher) {
  if (
    action.type === SYSTEM_MESSAGE &&
    action.payload?.message === 'WEEKLY_WEATHER_FETCHED'
  ) {
    const data = action.payload.data! as Array<DayWeatherJSON>;

    dispatch(setCurrentWeather(data));
  }
}
