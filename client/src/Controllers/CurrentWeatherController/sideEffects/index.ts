import { onWeeklyWeatherFetched } from './onWeeklyWeatherFetched';
import { onCitySelected } from './onCitySelected';

export const sideEffects = [onWeeklyWeatherFetched, onCitySelected];
