import { DayWeatherJSON } from 'Models/DayWeather';
import { Action } from 'StateManager';

import * as C from './constants';

export type SetCurrentWeatherAction = Action<
  typeof C.SET_CURRENT_WEATHER,
  Array<DayWeatherJSON>
>;
export function setCurrentWeather(data: Array<DayWeatherJSON>): SetCurrentWeatherAction {
  return { type: C.SET_CURRENT_WEATHER, payload: data };
}

export type SetIsLoadingAction = Action<typeof C.SET_IS_LOADING, boolean>;
export function setIsLoading(data: boolean): SetIsLoadingAction {
  return { type: C.SET_IS_LOADING, payload: data };
}

export type Actions = SetCurrentWeatherAction | SetIsLoadingAction;
