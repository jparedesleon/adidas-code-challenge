import { State } from 'StateManager';
import { Weather } from 'Models/Weather';

export const selectCurrentWeatherController = (state: State) =>
  state.currentWeatherController;

export interface CurrentWeatherFormatted {
  city: string;
  forecast: string;
  icon: Weather['icon'];
  temperature: string;
  date: string;
}

export const selectCurrentWeather = (state: State): CurrentWeatherFormatted | null => {
  const { currentWeather } = selectCurrentWeatherController(state);

  if (!currentWeather) {
    return null;
  }

  const currentHour = new Date();

  const currentHourWeather = currentWeather.hourlyTemperature.find((hourly) => {
    return hourly.hour.getHours() === currentHour.getHours();
  })!;

  if (!currentHourWeather) {
    return null;
  }

  return {
    city: currentWeather.location.city,
    forecast: currentHourWeather.weather.main,
    icon: currentHourWeather.weather.icon,
    temperature: `${Math.round(currentHourWeather?.temperature)}°`,
    date: `${currentWeather.date.toDateString()}`,
  };
};

export const selectIsLoading = (state: State): boolean => {
  const { isLoading } = selectCurrentWeatherController(state);

  return isLoading;
};
