import { Actions } from 'StateManager';
import { DayWeather } from 'Models/DayWeather';
import { normalizeDayWeatherJSON } from 'Models/normalizeData';

import type { Actions as LocalActions } from './actions';
import * as C from './constants';

export type WeeklyWeatherControllerState = {
  weeklyWeather: Array<DayWeather>;
  isLoading: boolean;
};

export type WeeklyWeatherControllerActions = LocalActions;
export const initialState: WeeklyWeatherControllerState = {
  weeklyWeather: [],
  isLoading: false,
};

export function reducer(
  state = initialState,
  action: Actions,
): WeeklyWeatherControllerState {
  switch (action.type) {
    case C.FETCH_WEEKLY_WEATHER: {
      return {
        ...state,
        isLoading: true,
      };
    }
    case C.FETCH_WEEKLY_WEATHER_SUCCESS: {
      const data = action.payload!;

      return {
        ...state,
        isLoading: false,
        weeklyWeather: data.map((weather) => normalizeDayWeatherJSON(weather)),
      };
    }
    case C.FETCH_WEEKLY_WEATHER_ERROR: {
      return state;
    }
    default: {
      return state;
    }
  }
}
