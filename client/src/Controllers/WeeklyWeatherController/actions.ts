import { Action } from 'StateManager';
import { DayWeatherJSON } from 'Models/DayWeather';

import * as C from './constants';

export type FetchWeeklyWeatherAction = Action<
  typeof C.FETCH_WEEKLY_WEATHER,
  { cityId: number }
>;
export function fetchWeeklyWeather(cityId: number): FetchWeeklyWeatherAction {
  return { type: C.FETCH_WEEKLY_WEATHER, payload: { cityId } };
}

export type FetchWeeklyWeatherSuccessAction = Action<
  typeof C.FETCH_WEEKLY_WEATHER_SUCCESS,
  Array<DayWeatherJSON>
>;
export function fetchWeeklyWeatherSuccess(
  data: Array<DayWeatherJSON>,
): FetchWeeklyWeatherSuccessAction {
  return {
    type: C.FETCH_WEEKLY_WEATHER_SUCCESS,
    payload: data,
  };
}

export type FetchWeeklyWeatherErrorAction = Action<typeof C.FETCH_WEEKLY_WEATHER_ERROR>;
export function fetchWeeklyWeatherError(): FetchWeeklyWeatherErrorAction {
  return {
    type: C.FETCH_WEEKLY_WEATHER_ERROR,
  };
}

export type Actions =
  | FetchWeeklyWeatherAction
  | FetchWeeklyWeatherSuccessAction
  | FetchWeeklyWeatherErrorAction;
