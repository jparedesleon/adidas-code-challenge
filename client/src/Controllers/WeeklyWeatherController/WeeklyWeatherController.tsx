import React, { useContext } from 'react';

import { emitSystemMessage, StateContext } from 'StateManager';
import { WeeklyWeather } from 'Components/WeeklyWeather';
import { HourlyWeatherController } from 'Controllers/HourlyWeatherController';

import { getDayWeather, selectWeeklyWeather, getIsLoading } from './selectors';

export function WeeklyWeatherController() {
  const { state, dispatch } = useContext(StateContext);
  const weeklyWeather = selectWeeklyWeather(state);
  const isLoading = getIsLoading(state);

  const handleWeatherClick = (id: number) => {
    dispatch(
      emitSystemMessage({
        message: 'WEEKLY_WEATHER_CLICKED',
        data: getDayWeather(state, id),
      }),
    );
  };

  return (
    <WeeklyWeather
      isLoading={isLoading}
      onWeatherClick={handleWeatherClick}
      data={weeklyWeather}
    >
      <HourlyWeatherController />
    </WeeklyWeather>
  );
}
