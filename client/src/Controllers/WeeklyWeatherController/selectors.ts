import { State } from 'StateManager';
import { DayWeather } from 'Models/DayWeather';
import { Weather } from 'Models/Weather';
import { getDayString, filterForNextFiveDays } from 'utils/time';

export const selectWeeklyWeatherController = (state: State) =>
  state.weeklyWeatherController;

export interface DayWeatherFormatted {
  id: number;
  date: string;
  day: string;
  maxTemperature: string;
  minTemperature: string;
  weather: Weather;
}

export const selectWeeklyWeather = (state: State): Array<DayWeatherFormatted> => {
  const { weeklyWeather } = selectWeeklyWeatherController(state);

  return weeklyWeather
    .filter((weather) => filterForNextFiveDays(weather.date))
    .map((weather) => {
      const MIDDAY_TIME = Math.floor(weather.hourlyTemperature.length / 2);

      return {
        id: weather.id,
        date: weather.date.toString(),
        day: getDayString(weather.date),
        maxTemperature: `${Math.round(weather.maxTemperature)}°`,
        minTemperature: `${Math.round(weather.minTemperature)}°`,
        weather: weather.hourlyTemperature[MIDDAY_TIME].weather,
      };
    });
};

export const getDayWeather = (state: State, id: number): DayWeather | undefined => {
  const { weeklyWeather } = selectWeeklyWeatherController(state);

  return weeklyWeather.find((weather) => weather.id === id);
};

export const getIsLoading = (state: State): boolean => {
  const { isLoading } = selectWeeklyWeatherController(state);

  return isLoading;
};
