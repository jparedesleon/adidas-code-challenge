import { Dispatcher, Actions, emitSystemMessage } from 'StateManager';
import { NetworkRequest } from 'utils/NetworkRequest';
import { SERVER_URL } from 'config';

import { fetchWeeklyWeatherSuccess } from '../actions';
import * as C from '../constants';

export async function onFetchWeeklyWeather(action: Actions, dispatch: Dispatcher) {
  if (action.type === C.FETCH_WEEKLY_WEATHER) {
    const id = action.payload?.cityId;
    const date = new Date();

    date.setHours(0, 0, 0, 0);

    const requester = new NetworkRequest(
      `${SERVER_URL}/api/daily-weather?city=${id}&date=${date.getTime() / 1000}`,
    );

    const request = await requester.sendRequest();

    const data = await request.json();

    dispatch(
      emitSystemMessage({
        message: 'WEEKLY_WEATHER_FETCHED',
        data,
      }),
    );
    dispatch(fetchWeeklyWeatherSuccess(data));
  }
}
