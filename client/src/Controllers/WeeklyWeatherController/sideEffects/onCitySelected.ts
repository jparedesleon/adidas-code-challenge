import { Dispatcher, Actions, SYSTEM_MESSAGE } from 'StateManager';
import { City } from 'Models/City';

import { fetchWeeklyWeather } from '../actions';

export async function onCitySelected(action: Actions, dispatch: Dispatcher) {
  if (
    action.type === SYSTEM_MESSAGE &&
    action.payload?.message === 'SEARCH_CITY_SELECTED'
  ) {
    const data = action.payload.data! as City;
    dispatch(fetchWeeklyWeather(data.id));
  }
}
