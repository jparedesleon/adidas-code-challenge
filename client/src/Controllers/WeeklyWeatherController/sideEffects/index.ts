import { onCitySelected } from './onCitySelected';
import { onFetchWeeklyWeather } from './onFetchWeeklyWeather';

export const sideEffects = [onCitySelected, onFetchWeeklyWeather];
