import { State } from 'StateManager';
import { Weather } from 'Models/Weather';
import { timeToHoursAndMinutes, filterForFutureHours } from 'utils/time';

export const selectHourlyWeatherController = (state: State) =>
  state.hourlyWeatherController;

export interface HourlyWeatherFormatted {
  id: string;
  time: string;
  forecast: string;
  icon: Weather['icon'];
  temperature: string;
}

export const selectHourlyWeather = (
  state: State,
): Array<HourlyWeatherFormatted> | null => {
  const { hourlyWeather } = selectHourlyWeatherController(state);

  return hourlyWeather
    .filter((weather) => filterForFutureHours(weather.hour))
    .map((weather) => ({
      id: weather.id,
      time: timeToHoursAndMinutes(weather.hour),
      forecast: weather.weather.main,
      icon: weather.weather.icon,
      temperature: `${Math.round(weather.temperature)}°`,
    }));
};

export const selectWeekDay = (state: State): string => {
  const { weekDay } = selectHourlyWeatherController(state);

  return weekDay;
};

export const selectActive = (state: State): boolean => {
  const { active } = selectHourlyWeatherController(state);

  return active;
};
