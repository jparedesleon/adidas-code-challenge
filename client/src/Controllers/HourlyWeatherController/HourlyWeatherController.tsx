import React, { useContext } from 'react';

import { StateContext } from 'StateManager';
import { HourlyWeather } from 'Components/HourlyWeather';

import { selectHourlyWeather, selectActive, selectWeekDay } from './selectors';

export function HourlyWeatherController() {
  const { state } = useContext(StateContext);
  const hourlyWeather = selectHourlyWeather(state);
  const active = selectActive(state);
  const weekDay = selectWeekDay(state);

  return <HourlyWeather weekDay={weekDay} active={active} data={hourlyWeather} />;
}
