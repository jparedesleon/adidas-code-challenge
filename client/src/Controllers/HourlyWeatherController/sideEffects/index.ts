import { onWeeklyWeatherClicked } from './onWeeklyWeatherClicked';
import { onCitySelected } from './onCitySelected';

export const sideEffects = [onWeeklyWeatherClicked, onCitySelected];
