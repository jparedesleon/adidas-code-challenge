import { Dispatcher, Actions, SYSTEM_MESSAGE } from 'StateManager';

import { setActive } from '../actions';

export async function onCitySelected(action: Actions, dispatch: Dispatcher) {
  if (
    action.type === SYSTEM_MESSAGE &&
    action.payload?.message === 'SEARCH_CITY_SELECTED'
  ) {
    dispatch(setActive(false));
  }
}
