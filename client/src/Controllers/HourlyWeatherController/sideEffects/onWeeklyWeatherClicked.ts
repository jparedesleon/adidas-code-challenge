import { Dispatcher, Actions, SYSTEM_MESSAGE } from 'StateManager';
import { DayWeather } from 'Models/DayWeather';

import { setHourlyWeather } from '../actions';

export async function onWeeklyWeatherClicked(action: Actions, dispatch: Dispatcher) {
  if (
    action.type === SYSTEM_MESSAGE &&
    action.payload?.message === 'WEEKLY_WEATHER_CLICKED'
  ) {
    const data = action.payload.data as DayWeather;

    if (data) {
      dispatch(setHourlyWeather(data));
    }
  }
}
