import { Actions } from 'StateManager';
import { HourWeather } from 'Models/HourWeather';
import { getDayString } from 'utils/time';

import type { Actions as LocalActions } from './actions';
import * as C from './constants';

export type HourlyWeatherState = {
  hourlyWeather: Array<HourWeather>;
  weekDay: string;
  active: boolean;
};

export type HourlyWeatherActions = LocalActions;
export const initialState: HourlyWeatherState = {
  hourlyWeather: [],
  weekDay: '',
  active: false,
};

export function reducer(state = initialState, action: Actions): HourlyWeatherState {
  switch (action.type) {
    case C.SET_HOURLY_WEATHER: {
      const dayWeather = action.payload!;

      return {
        ...state,
        active: true,
        weekDay: getDayString(dayWeather.date),
        hourlyWeather: dayWeather.hourlyTemperature,
      };
    }
    case C.SET_ACTIVE: {
      const active = action.payload!;

      return {
        ...state,
        active,
      };
    }
    default: {
      return state;
    }
  }
}
