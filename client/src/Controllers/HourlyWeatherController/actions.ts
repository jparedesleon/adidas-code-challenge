import { DayWeather } from 'Models/DayWeather';
import { Action } from 'StateManager';

import * as C from './constants';

export type SetHourlyWeatherAction = Action<typeof C.SET_HOURLY_WEATHER, DayWeather>;
export function setHourlyWeather(data: DayWeather): SetHourlyWeatherAction {
  return { type: C.SET_HOURLY_WEATHER, payload: data };
}

export type SetActiveAction = Action<typeof C.SET_ACTIVE, boolean>;
export function setActive(data: boolean): SetActiveAction {
  return { type: C.SET_ACTIVE, payload: data };
}

export type Actions = SetHourlyWeatherAction | SetActiveAction;
