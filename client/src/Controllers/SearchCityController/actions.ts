import { Action } from 'StateManager';
import { CityJSON } from 'Models/City';

import * as C from './constants';

export type FetchCitiesAction = Action<typeof C.FETCH_CITIES>;
export function fetchCities(): FetchCitiesAction {
  return { type: C.FETCH_CITIES };
}

export type FetchCitiesSuccessAction = Action<
  typeof C.FETCH_CITIES_SUCCESS,
  Array<CityJSON>
>;
export function fetchCitiesSuccess(data: Array<CityJSON>): FetchCitiesSuccessAction {
  return {
    type: C.FETCH_CITIES_SUCCESS,
    payload: data,
  };
}

export type FetchCitiesErrorAction = Action<typeof C.FETCH_CITIES_ERROR>;
export function fetchCitiesError(): FetchCitiesErrorAction {
  return {
    type: C.FETCH_CITIES_ERROR,
  };
}

export type Actions =
  | FetchCitiesAction
  | FetchCitiesSuccessAction
  | FetchCitiesErrorAction;
