import { Actions } from 'StateManager';
import { City } from 'Models/City';

import type { Actions as LocalActions } from './actions';
import * as C from './constants';

export type SearchCityControllerState = {
  cities: Array<City>;
  isLoading: boolean;
};

export type SearchCityControllerActions = LocalActions;
export const initialState: SearchCityControllerState = {
  cities: [],
  isLoading: false,
};

export function reducer(
  state = initialState,
  action: Actions,
): SearchCityControllerState {
  switch (action.type) {
    case C.FETCH_CITIES: {
      return {
        ...state,
        isLoading: true,
      };
    }
    case C.FETCH_CITIES_SUCCESS: {
      const data = action.payload!;

      return {
        ...state,
        isLoading: false,
        cities: data,
      };
    }
    case C.FETCH_CITIES_ERROR: {
      return state;
    }
    default: {
      return state;
    }
  }
}
