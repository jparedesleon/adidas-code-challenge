import React, { useContext, useEffect } from 'react';

import { StateContext, emitSystemMessage } from 'StateManager';
import { City } from 'Models/City';
import { SearchCity } from 'Components/SearchCity';

import { fetchCities } from './actions';
import { selectCities, selectIsLoading } from './selectors';

export function SearchCityController() {
  const { state, dispatch } = useContext(StateContext);
  const cities = selectCities(state);
  const isLoading = selectIsLoading(state);

  const handleCityChange = (city: City) => {
    dispatch(
      emitSystemMessage({
        message: 'SEARCH_CITY_SELECTED',
        data: city,
      }),
    );
  };

  useEffect(() => {
    dispatch(fetchCities());
  }, []);

  return (
    <SearchCity isLoading={isLoading} onCityChange={handleCityChange} cities={cities} />
  );
}
