import { Dispatcher, Actions } from 'StateManager';
import { NetworkRequest } from 'utils/NetworkRequest';
import { SERVER_URL } from 'config';

import { fetchCitiesSuccess } from '../actions';
import * as C from '../constants';

export async function onFetchCities(action: Actions, dispatch: Dispatcher) {
  if (action.type === C.FETCH_CITIES) {
    const requester = new NetworkRequest(`${SERVER_URL}/api/cities`);

    const request = await requester.sendRequest();

    const data = await request.json();

    dispatch(fetchCitiesSuccess(data));
  }
}
