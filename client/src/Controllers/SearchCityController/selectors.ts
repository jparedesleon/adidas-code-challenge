import { State } from 'StateManager';
import { City } from 'Models/City';

export const selectSearchCityController = (state: State) => state.searchCityController;

export const selectCities = (state: State): Array<City> => {
  const searchCity = selectSearchCityController(state);

  return searchCity.cities;
};

export const selectCity = (state: State, id: City['id']): City | undefined => {
  const searchCity = selectSearchCityController(state);

  return searchCity.cities.find((city) => city.id === id);
};

export const selectIsLoading = (state: State): boolean => {
  const { isLoading } = selectSearchCityController(state);

  return isLoading;
};
