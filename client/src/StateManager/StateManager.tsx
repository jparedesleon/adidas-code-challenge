import React, { useReducer, createContext } from 'react';

import {
  State,
  initialState,
  Actions,
  getCombinedReducers,
  sideEffects,
} from './entries';
import * as T from './types';

let dispatcher: T.Dispatcher = () => {};

export const StateContext = createContext<T.StateContext>({
  state: initialState,
  dispatch: dispatcher,
});

export const reducers = (state: State, action: Actions): State => {
  if (process.env.NODE_ENV === 'development') {
    // eslint-disable-next-line no-console
    console.log(
      '%cStateManager:',
      'color: peru; font-size: 15px; font-weight: bold;',
      action,
      state,
    );
  }
  const combinedReducers = getCombinedReducers(state, action);

  sideEffects.forEach((sideEffect) => sideEffect(action, dispatcher));

  return combinedReducers;
};

export function StateManager({ children }: React.PropsWithChildren<{}>) {
  const [state, dispatch] = useReducer(reducers, initialState);

  if (dispatcher !== dispatch) {
    dispatcher = dispatch;
  }

  return (
    <StateContext.Provider value={{ state, dispatch }}>{children}</StateContext.Provider>
  );
}
