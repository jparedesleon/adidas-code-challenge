import { selectors as searchCitySelectors } from 'Controllers/SearchCityController';
import { selectors as weeklyWeatherSelectors } from 'Controllers/WeeklyWeatherController';

export default { searchCitySelectors, weeklyWeatherSelectors };
