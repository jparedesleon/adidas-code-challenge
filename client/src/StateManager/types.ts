import { State, Actions } from './entries';

export type Dispatcher = (action: Actions) => void;

export type Action<T extends string, P = undefined> = {
  type: T;
  payload?: P;
};

export type StateContext = {
  state: State;
  dispatch: (a: Actions) => void;
};
