import * as searchCity from 'Controllers/SearchCityController';
import * as weeklyWeather from 'Controllers/WeeklyWeatherController';
import * as currentWeather from 'Controllers/CurrentWeatherController';
import * as hourlyWeather from 'Controllers/HourlyWeatherController';

import { EmitSystemMessageAction } from './actions';

export type State = {
  searchCityController: searchCity.SearchCityControllerState;
  weeklyWeatherController: weeklyWeather.WeeklyWeatherControllerState;
  currentWeatherController: currentWeather.CurrentWeatherState;
  hourlyWeatherController: hourlyWeather.HourlyWeatherState;
};

export const initialState: State = {
  searchCityController: searchCity.initialState,
  weeklyWeatherController: weeklyWeather.initialState,
  currentWeatherController: currentWeather.initialState,
  hourlyWeatherController: hourlyWeather.initialState,
};

export type Actions =
  | EmitSystemMessageAction
  | weeklyWeather.Actions
  | currentWeather.Actions
  | hourlyWeather.Actions
  | searchCity.Actions;

export const getCombinedReducers = (state: State, action: Actions) => ({
  searchCityController: searchCity.reducer(state.searchCityController, action),
  weeklyWeatherController: weeklyWeather.reducer(state.weeklyWeatherController, action),
  currentWeatherController: currentWeather.reducer(
    state.currentWeatherController,
    action,
  ),
  hourlyWeatherController: hourlyWeather.reducer(state.hourlyWeatherController, action),
});

export const sideEffects = [
  ...searchCity.sideEffects,
  ...weeklyWeather.sideEffects,
  ...currentWeather.sideEffects,
  ...hourlyWeather.sideEffects,
];

export type SystemMessages = searchCity.SystemMessages | weeklyWeather.SystemMessages;
