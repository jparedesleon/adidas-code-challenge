import { Action } from './types';
import { SystemMessages } from './entries';
import * as C from './constants';

type EmitSystemMessagePayload = {
  message: SystemMessages;
  data?: unknown;
};
export type EmitSystemMessageAction = Action<
  typeof C.SYSTEM_MESSAGE,
  EmitSystemMessagePayload
>;
export function emitSystemMessage(
  payload: EmitSystemMessagePayload,
): EmitSystemMessageAction {
  return {
    type: C.SYSTEM_MESSAGE,
    payload,
  };
}
