import { City } from "./City";
import { HourlyWeather } from "./HourlyWeather";

export interface DailyWeather {
  id: string;
  date: { seconds: number };
  city_id: City["id"];
  min_temperature: number;
  max_temperature: number;
  hourly_temperature: Array<HourlyWeather["id"]>;
}

export interface DailyWeatherOW {
  id: string;
  date: Date;
  city_id: City["id"];
  min_temperature: number;
  max_temperature: number;
  hourly_temperature: Array<HourlyWeather["id"]>;
}
