import { Weather } from "./Weather";

export interface HourlyWeather {
  id: string;
  time: { seconds: number };
  temperature: number;
  weather_id: Weather["id"];
}

export interface HourlyWeatherOW {
  id: string;
  time: Date;
  temperature: number;
  weather_id: Weather["id"];
}
