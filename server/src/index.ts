import express from "express";
import cors from "cors";
import admin, { ServiceAccount } from "firebase-admin";
import { cert } from "./config/cert";
import { citiesRoutes } from "./routes/cities";
import { dailyWeatherRoutes } from "./routes/daily-weather";
import { weatherRoutes } from "./routes/weather";
import { hourlyWeatherRoutes } from "./routes/hourly-weather";

try {
  admin.initializeApp({
    credential: admin.credential.cert(cert as ServiceAccount),
  });
} catch (error) {
  console.log("Invalid credentials 😓😓", error);
}

const app = express();
const port = process.env.PORT || 3001;
const db = admin.firestore();

app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(cors());

app.get("/", (_, res) => res.send("welcome to the weather api"));

citiesRoutes(app, db);
weatherRoutes(app, db);
dailyWeatherRoutes(app, db);
hourlyWeatherRoutes(app, db);

app.listen(port, () => {
  // eslint-disable-next-line no-console
  console.log(`🚀 server started at http://localhost:${port}`);
});
