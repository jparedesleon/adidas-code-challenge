import { Request, Response } from "express";
import { HourlyWeather } from "../../models/HourlyWeather";

export async function update(
  req: Request,
  res: Response,
  db: FirebaseFirestore.Firestore
) {
  try {
    await db
      .collection("hourly-weather")
      .doc(req.params.id)
      .update({
        time: req.body.time,
        temperature: req.body.temperature,
        weather_id: req.body.weatherId,
      } as HourlyWeather);

    return res.status(200).send();
  } catch (error) {
    console.error("Error updating:", error);
    return res.status(500).send();
  }
}
