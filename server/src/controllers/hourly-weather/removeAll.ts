import { Request, Response } from "express";

export async function removeAll(
  req: Request,
  res: Response,
  db: FirebaseFirestore.Firestore
) {
  try {
    const collection = await db.collection("hourly-weather").get();

    collection.forEach((doc) => {
      doc.ref.delete();
    });

    return res.send();
  } catch (error) {
    console.error("Error deleting:", error);
    return res.status(500).send();
  }
}
