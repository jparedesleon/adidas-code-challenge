import { Request, Response } from "express";
import { HourlyWeather } from "../../models/HourlyWeather";

export async function findAll(
  req: Request,
  res: Response,
  db: FirebaseFirestore.Firestore
) {
  try {
    const document = db.collection("hourly-weather");
    const collections = await document.get();
    const { docs } = collections;
    const response: any[] = [];

    // eslint-disable-next-line no-restricted-syntax
    for await (const doc of docs) {
      const hourlyWeather = doc.data() as HourlyWeather;
      const weather = await db
        .collection("weather")
        .doc(hourlyWeather.weather_id)
        .get();

      response.push({
        hour: hourlyWeather.time.seconds,
        temperature: hourlyWeather.temperature,
        weather: { id: weather.id, ...weather.data() },
      });
    }

    return res.status(200).send(response);
  } catch (error) {
    console.error("Error updating querying all:", error);
    return res.status(500).send(error);
  }
}
