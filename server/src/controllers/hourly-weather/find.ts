import { Request, Response } from "express";
import { HourlyWeather } from "../../models/HourlyWeather";

export async function find(
  req: Request,
  res: Response,
  db: FirebaseFirestore.Firestore
) {
  try {
    const collection = db.collection("hourly-weather");
    const hourlyWeatherDoc = await collection.doc(req.params.id).get();
    const hourlyWeather = hourlyWeatherDoc.data() as HourlyWeather;

    if (!hourlyWeather) {
      return res.status(400).send();
    }

    const weatherDoc = await db
      .collection("weather")
      .doc(hourlyWeather.weather_id)
      .get();

    return res.status(200).send({
      id: hourlyWeatherDoc.id,
      hour: hourlyWeather.time.seconds,
      temperature: hourlyWeather.temperature,
      weather: { id: weatherDoc.id, ...weatherDoc.data() },
    });
  } catch (error) {
    console.error("Error updating querying by id:", error);
    return res.status(500).send(error);
  }
}
