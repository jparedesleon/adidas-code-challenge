import { Request, Response } from "express";

export async function findAll(
  req: Request,
  res: Response,
  db: FirebaseFirestore.Firestore
) {
  try {
    const document = db.collection("cities");
    const collections = await document.get();
    const { docs } = collections;
    const response: any[] = [];

    Object.values(docs).forEach((doc) => {
      response.push({
        id: doc.id,
        city: doc.data().city,
        country: doc.data().country,
      });
    });

    return res.status(200).send(response);
  } catch (error) {
    console.error("Error updating querying all:", error);
    return res.status(500).send(error);
  }
}
