import { Request, Response } from "express";

export async function update(
  req: Request,
  res: Response,
  db: FirebaseFirestore.Firestore
) {
  try {
    await db.collection("cities").doc(req.params.id).update({
      city: req.body.city,
      country: req.body.country,
    });

    return res.status(200).send();
  } catch (error) {
    console.error("Error updating:", error);
    return res.status(500).send();
  }
}
