import { Request, Response } from "express";

export async function create(
  req: Request,
  res: Response,
  db: FirebaseFirestore.Firestore
) {
  try {
    await db.collection("cities").doc(req.body.id).create({
      city: req.body.city,
      country: req.body.country,
    });

    return res.status(200).send();
  } catch (error) {
    console.error("Error creating:", error);
    return res.status(500).send();
  }
}
