import { Request, Response } from "express";

export async function findAll(
  req: Request,
  res: Response,
  db: FirebaseFirestore.Firestore
) {
  try {
    const document = db.collection("weather");
    const collections = await document.get();
    const { docs } = collections;
    const response: any[] = [];

    Object.values(docs).forEach((doc) => {
      response.push({
        id: doc.id,
        main: doc.data().main,
        description: doc.data().description,
        icon: doc.data().icon,
      });
    });

    return res.status(200).send(response);
  } catch (error) {
    console.error("Error updating querying all:", error);
    return res.status(500).send(error);
  }
}
