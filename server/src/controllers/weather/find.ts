import { Request, Response } from "express";

export async function find(
  req: Request,
  res: Response,
  db: FirebaseFirestore.Firestore
) {
  try {
    const collection = db.collection("weather");
    const document = collection.doc(req.params.id);
    const city = await document.get();
    const reponse = city.data();

    return res.status(200).send(reponse);
  } catch (error) {
    console.error("Error updating querying by id:", error);
    return res.status(500).send(error);
  }
}
