import { Request, Response } from "express";

export async function create(
  req: Request,
  res: Response,
  db: FirebaseFirestore.Firestore
) {
  try {
    await db.collection("weather").doc(req.body.id).create({
      main: req.body.main,
      description: req.body.description,
      icon: req.body.icon,
    });

    return res.status(200).send();
  } catch (error) {
    console.error("Error creating:", error);
    return res.status(500).send();
  }
}
