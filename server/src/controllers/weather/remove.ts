import { Request, Response } from "express";

export async function remove(
  req: Request,
  res: Response,
  db: FirebaseFirestore.Firestore
) {
  try {
    await db.collection("weather").doc(req.params.id).delete();

    return res.status(200).send();
  } catch (error) {
    console.error("Error deleting:", error);
    return res.status(500).send();
  }
}
