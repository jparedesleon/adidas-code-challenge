import { Request, Response } from "express";

export async function update(
  req: Request,
  res: Response,
  db: FirebaseFirestore.Firestore
) {
  try {
    await db.collection("daily-weather").doc(req.params.id).update({
      date: req.body.date,
      hourly_temperature: req.body.hourly_temperature,
    });

    return res.status(200).send();
  } catch (error) {
    console.error("Error updating:", error);
    return res.status(500).send();
  }
}
