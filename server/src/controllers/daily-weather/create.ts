import { Request, Response } from "express";

export async function create(
  req: Request,
  res: Response,
  db: FirebaseFirestore.Firestore
) {
  try {
    await db.collection("daily-weather").doc(req.body.id).create({
      date: req.body.date,
      hourly_temperature: req.body.hourly_temperature,
    });

    return res.status(200).send();
  } catch (error) {
    console.error("Error creating:", error);
    return res.status(500).send();
  }
}
