import { Request, Response } from "express";
import { DailyWeather } from "../../models/DailyWeather";

export async function find(
  req: Request,
  res: Response,
  db: FirebaseFirestore.Firestore
) {
  try {
    const collection = db.collection("daily-weather").doc(req.params.id);

    const hourlyHours: any = [];
    const dailyWeatherDoc = await collection.get();
    const dailyWeather = dailyWeatherDoc.data() as DailyWeather;

    if (!dailyWeather) {
      return res.status(400).send();
    }

    const { city_id } = dailyWeather;

    const city = await db.collection("cities").doc(city_id).get();

    // eslint-disable-next-line no-restricted-syntax
    for await (const id of dailyWeather.hourly_temperature) {
      const hourlyDoc = await db.collection("hourly-weather").doc(id).get();
      const hourly = hourlyDoc.data();
      const { weatherId } = hourly || {};

      if (!hourly) {
        return res.status(400).send();
      }

      const weather = await db.collection("weather").doc(weatherId).get();

      hourlyHours.push({
        id: hourlyDoc.id,
        hour: hourly.time.seconds,
        temperature: hourly.temperature,
        weather: { id: weather.id, ...weather.data() },
      });
    }

    return res.send({
      id: dailyWeatherDoc.id,
      date: dailyWeather.date.seconds,
      max_temperature: dailyWeather.max_temperature,
      min_temperature: dailyWeather.min_temperature,
      location: { id: city.id, ...city.data() },
      hourly_temperature: hourlyHours,
    });
  } catch (error) {
    console.error("Error updating querying by id:", error);
    return res.status(500).send(error);
  }
}
