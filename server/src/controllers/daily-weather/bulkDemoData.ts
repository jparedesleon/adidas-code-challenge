import axios from "axios";
import hash from "object-hash";
import { Request, Response } from "express";
import { getOWCitiesUrl, getOWDailyUrl } from "../../config/openWeatherApi";

import { DailyWeatherOW } from "../../models/DailyWeather";
import { City } from "../../models/City";
import { Weather } from "../../models/Weather";
import { HourlyWeatherOW } from "../../models/HourlyWeather";

export async function bulkDemoData(
  req: Request,
  res: Response,
  db: FirebaseFirestore.Firestore
) {
  try {
    const cityCollection = await db.collection("cities").get();
    const dailyWeather: Record<string, any> = {};
    const hourlyWeather: Record<string, any> = {};
    const hoursToDate: Record<number, any> = {};

    for await (const cityDoc of cityCollection.docs) {
      const city = cityDoc.data() as City;

      const responseCity = await axios.get(getOWCitiesUrl(city.city));
      const owCity = responseCity.data;
      const { lat, lon } = owCity.coord;
      const responseDaily = await axios.get(getOWDailyUrl(lat, lon));
      dailyWeather[cityDoc.id] = responseDaily.data.daily;
      hourlyWeather[cityDoc.id] = responseDaily.data.hourly;
    }

    for await (const [city, hours] of Object.entries(hourlyWeather)) {
      for await (const hour of hours) {
        const hourDate = new Date(hour.dt * 1000);
        const dayDate = new Date(hourDate.getTime());
        const hourHashId = hash({ city, ...hour });

        dayDate.setHours(0, 0, 0, 0);

        await db
          .collection("hourly-weather")
          .doc(hourHashId)
          .set({
            time: hourDate,
            temperature: hour.temp,
            weather_id: hour.weather[0].id.toString(),
          } as Omit<HourlyWeatherOW, "id">);

        await db
          .collection("weather")
          .doc(hour.weather[0].id.toString())
          .set({
            icon: hour.weather[0].icon,
            description: hour.weather[0].description,
            main: hour.weather[0].main,
          } as Omit<Weather, "id">);

        const dayDateTimestamp = dayDate.getTime() / 1000;

        if (!hoursToDate[dayDateTimestamp]) hoursToDate[dayDateTimestamp] = [];

        hoursToDate[dayDateTimestamp].push(hourHashId);
      }
    }

    for await (const [city, days] of Object.entries(dailyWeather)) {
      for await (const day of days) {
        const dayDate = new Date(day.dt * 1000);

        dayDate.setHours(0, 0, 0, 0);

        await db
          .collection("daily-weather")
          .doc(hash({ city, ...day }))
          .set({
            city_id: city,
            date: dayDate,
            max_temperature: day.temp.max,
            min_temperature: day.temp.min,
            hourly_temperature: hoursToDate[dayDate.getTime() / 1000] || [],
          } as Omit<DailyWeatherOW, "id">);
      }
    }

    return res.status(200).send();
  } catch (error) {
    console.error("Error creating:", error);
    return res.status(500).send();
  }
}
