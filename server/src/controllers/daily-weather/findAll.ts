import { Request, Response } from "express";

import { DailyWeather } from "../../models/DailyWeather";
import { HourlyWeather } from "../../models/HourlyWeather";
import { Weather } from "../../models/Weather";

export async function findAll(
  req: Request,
  res: Response,
  db: FirebaseFirestore.Firestore
) {
  try {
    const document = db.collection("daily-weather");
    const date = new Date(Date.now());

    date.setHours(0, 0, 0, 0);

    const collections = await document
      .orderBy("date")
      .where("city_id", "==", req.query.city)
      .startAt(date.getTime() / 1000)
      .limit(7)
      .get();
    const { docs } = collections;
    const hourlyWeatherList: any[] = [];
    const dailyWetaherList: any[] = [];

    for await (const doc of docs) {
      const daily = doc.data() as DailyWeather;
      const cityDoc = await db.collection("cities").doc(daily.city_id).get();

      for await (const hourlyId of daily.hourly_temperature) {
        const hourlyDoc = await db
          .collection("hourly-weather")
          .doc(hourlyId)
          .get();

        const hourly = hourlyDoc.data() as HourlyWeather;
        const weatherDoc = await db
          .collection("weather")
          .doc(hourly.weather_id)
          .get();
        const weather = weatherDoc.data() as Omit<Weather, "id">;

        hourlyWeatherList.push({
          id: hourlyDoc.id,
          hour: hourly.time.seconds,
          temperature: hourly.temperature,
          weather: { id: weatherDoc.id, ...weather },
        });
      }

      dailyWetaherList.push({
        id: doc.id,
        date: daily.date.seconds,
        min_temperature: daily.min_temperature,
        max_temperature: daily.max_temperature,
        location: { id: cityDoc.id, ...cityDoc.data() },
        hourly_temperature: hourlyWeatherList,
      });
    }

    return res.send(dailyWetaherList);
  } catch (error) {
    return res.status(500).send(error);
  }
}
