export const URL = "https://api.openweathermap.org/data/2.5/";
export const openWeatherKey = process.env.OPEN_WEATHER_API_KEY;

export function getOWCitiesUrl(city: string) {
  return `${URL}/weather?q=${city.toLocaleLowerCase()}&units=metric&apikey=${openWeatherKey}`;
}

export function getOWDailyUrl(lat: string, lon: string) {
  return `${URL}/onecall?lat=${lat}&lon=${lon}&exclude=minutely&units=metric&apikey=${openWeatherKey}`;
}
