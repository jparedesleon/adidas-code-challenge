import express, { Application } from "express";

import * as controller from "../controllers/hourly-weather";

export const hourlyWeatherRoutes = (
  app: Application,
  db: FirebaseFirestore.Firestore
) => {
  const router = express.Router();

  router.post("/", (req, res) => controller.create(req, res, db));

  router.get("/", (req, res) => controller.findAll(req, res, db));

  router.get("/:id", (req, res) => controller.find(req, res, db));

  router.put("/:id", (req, res) => controller.update(req, res, db));

  router.delete("/:id", (req, res) => controller.remove(req, res, db));

  router.delete("/", (req, res) => controller.removeAll(req, res, db));

  app.use("/api/hourly-weather", router);
};
