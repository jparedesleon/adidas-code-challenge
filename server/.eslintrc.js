module.exports = {
  root: true,
  env: {
    es6: true,
    node: true,
  },
  extends: ["airbnb-base", "plugin:prettier/recommended"],
  parser: "@typescript-eslint/parser",
  parserOptions: {
    project: ["tsconfig.json", "tsconfig.dev.json"],
    sourceType: "module",
  },
  ignorePatterns: [
    "/lib/**/*", // Ignore built files.
  ],
  settings: {
    "import/resolver": {
      node: {
        paths: ["src"],
        extensions: [".js", ".ts"],
      },
    },
  },
  plugins: ["@typescript-eslint", "import", "prettier"],
  rules: {
    quotes: ["error", "double"],
    camelcase: "off",
    "prettier/prettier": "error",
    "import/extensions": ["error", "ignorePackages", { ts: "never" }],
    "import/prefer-default-export": "off",
    "class-methods-use-this": "off",
    "no-unused-expressions": "off",
    "no-restricted-syntax": "off",
    "no-debugger": "warn",
    "@typescript-eslint/no-unused-vars": [
      "warn",
      {
        args: "none",
      },
    ],
  },
};
