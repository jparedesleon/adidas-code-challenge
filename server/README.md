## Table of Contents

- [Setup](#setup)
- [Project overview](#project-overview)
- [Acknowledgement of the project](#acknowledgement)
- [Run the project](#run-project)
- [Production mode](#production)
- [Testing](#testing)

## Setup

The requirements to setup the project are:

- Required Node version: **>=v12.13.0**
- Required Npm version: **>=6.12.0**

Installing dependencies:

```sh
npm install
```

## Project overview

The technologies used in this project are

- **Typescript**: for type safety, I think typescript is a must on the backend side
- **ESLint, Prettier and editorconfig**: The combination of these three results in a cross IDE, autoformatted code style guidelines which improve developer time and also code quality necessary for any type of project.
- **Express**: for our webserver provider.
- **Firebase**: Could service database to manage all the data persistence.

1. Erasing all the weather data. // send a DELETE request `<domain>/api/daily-weather`
2. Adding new weather data. // send a POST request to `<domain>/api/daily-weather`
3. Returning all the weather data. // send a GET request to `<domain>/api/daily-weather`
4. Returning the weather data filtered by date and location. // TBD

### API

    - cities:
        GET `<domain>/api/cities`: get all the cities
        GET `<domain>/api/cities/:id`: get one city by ID
        POST `<domain>/api/cities`: Expected JSON body { id: string, country: string, city: string } e.g:
            {
                "id": "4",
                "city": "Valencia",
                "country": "Spain"
            }
        PUT `<domain>/api/cities/:id`: Expected JSON body { country: string, city: string }
        DELETE `<domain>/api/cities/:id`: delete one city registry by ID
        DELETE `<domain>/api/cities`: delete all the cities

    - weather:
        GET `<domain>/api/weather`: get all the weather
        GET `<domain>/api/weather/:id`: get one weather registry by ID
        POST `<domain>/api/weather`: Expected JSON body { id: string, main: string, description: string, icon: string } e.g.:
            {
                "id": "4",
                "main": "Clear",
                "description": "clear",
                "icon": "01n"
            }
        PUT `<domain>/api/weather/:id`: Expected JSON body { main: string, description: string, icon: string }
        DELETE `<domain>/api/weather/:id`: delete one weather registry by ID
        DELETE `<domain>/api/weather`: delete all the weather

    - hourly-weather:
        GET `<domain>/api/hourly-weather`: get all the hourly weather
        GET `<domain>/api/hourly-weather/:id`: get one hourly weather registry by ID
        POST `<domain>/api/hourly-weather`: Expected JSON body { id: string, time: timestamp, temperature: number, weather_id: string } e.g.:
            {
                "id": "8",
                "time": 1624527960,
                "temperature": 31,
                "weather_id": "1"
            }
        PUT `<domain>/api/hourly-weather/:id`: Expected JSON body { time: timestamp, temperature: number, weatherId: string }
        DELETE `<domain>/api/hourly-weather/:id`: delete one hourly weather registry by ID
        DELETE `<domain>/api/hourly-weather`: delete all the hourly weather

    - daily-weather:
        GET `<domain>/api/daily-weather`: get all the daily weather
        GET `<domain>/api/daily-weather/:id`: get one daily weather registry by ID
        POST `<domain>/api/daily-weather`: Expected JSON body { id: string, date: timestamp, max_temperature: number, min_temperature: number, hourly_temperature: Array<string> } e.g.:
            {
                "id": "1",
                "date": 1624527960,
                "max_temperature": 20,
                "min_temperature": 30,
                "city_id": "1",
                "hourly_temperature": ["1", "2"]
            }
        PUT `<domain>/api/daily-weather/:id`: Expected JSON body { time: timestamp, temperature: number, weatherId: string }
        DELETE `<domain>/api/daily-weather/:id`: delete one daily weather registry by ID
        DELETE `<domain>/api/daily-weather`: delete all the daily weather

## MUST DO

- Allow `api/daily-weather` to filter by **city** and by **date**
- Provide an endpoint to populate the database with demo data fetched from **openweatherapi**
- Data validation
- Proper HTTP codes manipulation
- Improve getAll daily-weather algorithm
- Apply SOLID principles
- Swagger for API documentation
- Real-time application

## Acknowledgement

- Due to the time constrain of the project I wanted to focus more on the frontend which is my area of expertise and have a more flexible backend but well structured, I'm aware that there is a lot that can be improved and I will continue working on the project
- Firebase Database query efficiency could have definitely been better, and I will work the improvements
- I've attached the simplest model for the database, it could also be an area of improvement

## Run project

Before running the project locally you need to create the **.env** file with the following keys:

FIREBASE_CERT_TYPE  
FIREBASE_CERT_PROJECT_ID  
FIREBASE_CERT_PRIVATE_KEY_ID  
FIREBASE_CERT_PRIVATE_KEY  
FIREBASE_CERT_CLIENT_EMAIL  
FIREBASE_CERT_CLIENT_ID  
FIREBASE_CERT_AUTH_URI  
FIREBASE_CERT_TOKEN_URI  
FIREBASE_CERT_AUTH_PROVIDER  
FIREBASE_CERT_CLIENT

Please send me an email to joseaplwork@gmail.com so I can provide you the credentials for firebase database or you can just try app deployed to production

To start the project run:

```sh
npm run start:dev
```

> Note: you need to have the **.env** file created before starting the project

## Production

Visit: https://weather-app-adidas-api.herokuapp.com/
